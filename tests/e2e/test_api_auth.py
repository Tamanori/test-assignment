import pytest
from fastapi import status
from src.utils.auth import create_refresh_token
from tests.conftest import USER


class TestGetAccessToken:
    @pytest.mark.usefixtures("create_user")
    async def test_get_token(self, client_app):
        response = await client_app.post(
            "/auth/token",
            data=USER.dict(),
            headers={"content-type": "application/x-www-form-urlencoded"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert "access_token" in response.json().keys()
        assert "refresh_token" in response.json().keys()

    async def test_get_token_user_not_exist(self, client_app):
        response = await client_app.post(
            "/auth/token",
            data=USER.dict(),
            headers={"content-type": "application/x-www-form-urlencoded"},
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND
        assert response.json() == {"detail": "user with given username does not exist"}

    @pytest.mark.usefixtures("create_user")
    async def test_get_token_wrong_password(self, client_app):
        USER.password = "wrong"
        response = await client_app.post(
            "auth/token",
            data=USER.dict(),
            headers={"content-type": "application/x-www-form-urlencoded"},
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "Could not validate credentials"}


class TestGetRefreshToken:
    @pytest.mark.usefixtures("create_user")
    async def test_refresh_token(self, client_app):
        token = create_refresh_token(identity=USER.username)
        response = await client_app.post(
            "/auth/refresh",
            data={"refresh_token": token},
            headers={"content-type": "application/x-www-form-urlencoded"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert "access_token" in response.json()

    async def test_refresh_token_user_not_exist(self, client_app):
        token = create_refresh_token(identity=USER.username)
        response = await client_app.post(
            "/auth/refresh",
            data={"refresh_token": token},
            headers={"content-type": "application/x-www-form-urlencoded"},
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND
        assert response.json() == {"detail": "user with given username does not exist"}
