from sqlalchemy import select
import pytest
from httpx import AsyncClient
from src.db.base import create_db_session
from src.db.models.user import UserTable
from src.main import create_app
from src.utils.auth import get_password_hash
from tests.conftest import USER


@pytest.fixture
async def session_wrapper(class_session_factory):
    async def wrapper():
        return class_session_factory

    return wrapper


@pytest.fixture
async def client_app(session_wrapper):
    app = create_app()
    app.dependency_overrides[create_db_session] = session_wrapper
    async with AsyncClient(app=app, base_url="http://test") as c:
        yield c


@pytest.fixture
async def create_user(class_session_factory):
    async with class_session_factory() as session:
        u = UserTable(
            id=1,
            username=USER.username,
            password=get_password_hash(USER.password),
            salary=USER.salary,
            promotion_date=USER.promotion_date,
        )
        session.add(u)
        await session.commit()


async def get_user(client_session_factory, username):
    async with client_session_factory() as session:
        query = select(UserTable).where(UserTable.username == username)
        result = await session.execute(query)
        return result.scalars().first()
