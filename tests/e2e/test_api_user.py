from datetime import date, datetime, timedelta
from jose import jwt
import pytest
from fastapi import status
from src.schemas.user import UserUpdate
from src.utils.auth import create_access_token
from tests.conftest import USER
from tests.e2e.conftest import get_user


class TestCreateUser:
    async def test_create_user(self, client_app):
        response = await client_app.post(
            "/users", data=USER.json(), headers={"Content-Type": "application/json"}
        )
        assert response.status_code == status.HTTP_201_CREATED
        assert "access_token" in response.json().keys()
        assert "refresh_token" in response.json().keys()

    @pytest.mark.usefixtures("create_user")
    async def test_create_user_already_exist(self, client_app):
        response = await client_app.post(
            "/users", data=USER.json(), headers={"Content-Type": "application/json"}
        )
        assert response.status_code == status.HTTP_409_CONFLICT

    async def test_create_user_without_username(self, client_app):
        response = await client_app.post(
            "/users",
            data=USER.json(
                exclude={
                    "username",
                }
            ),
            headers={"Content-Type": "application/json"},
        )
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY

    async def test_create_user_without_password(self, client_app):
        response = await client_app.post(
            "/users",
            data=USER.json(
                exclude={
                    "password",
                }
            ),
            headers={"Content-Type": "application/json"},
        )
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY

    async def test_create_user_without_optional_data(self, client_app):
        response = await client_app.post(
            "/users",
            data=USER.json(exclude={"salary", "promotion_date"}),
            headers={"Content-Type": "application/json"},
        )
        assert response.status_code == status.HTTP_201_CREATED
        assert "access_token" in response.json().keys()
        assert "refresh_token" in response.json().keys()


class TestGetMe:
    @pytest.mark.usefixtures("create_user")
    async def test_get_me(self, client_app):
        token = create_access_token(identity=USER.username)
        response = await client_app.get(
            "/users/me",
            headers={
                "content-type": "application/x-www-form-urlencoded",
                "Authorization": f"Bearer {token}",
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json()["username"] == USER.username
        assert response.json()["salary"] == USER.salary
        assert response.json()["promotion_date"] == USER.promotion_date.strftime(
            "%Y-%m-%d"
        )

    @pytest.mark.usefixtures("create_user")
    async def test_get_me_not_username(self, client_app, settings):
        token = jwt.encode(
            {
                "exp": datetime.utcnow()
                + timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES),
            },
            settings.SECRET_KEY,
            algorithm=settings.ALGORITHM,
        )
        response = await client_app.get(
            "/users/me",
            headers={
                "content-type": "application/x-www-form-urlencoded",
                "Authorization": f"Bearer {token}",
            },
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "Could not validate credentials"}

    async def test_get_me_jwt_error(
        self,
        client_app,
    ):
        response = await client_app.get(
            "/users/me",
            headers={
                "content-type": "application/x-www-form-urlencoded",
                "Authorization": "Bearer error",
            },
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        assert response.json() == {"detail": "Could not validate credentials"}

    async def test_get_me_user_not_exist(self, client_app, settings):
        token = jwt.encode(
            {
                "sub": "test",
                "exp": datetime.utcnow()
                + timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES),
            },
            settings.SECRET_KEY,
            algorithm=settings.ALGORITHM,
        )
        response = await client_app.get(
            "/users/me",
            headers={
                "content-type": "application/x-www-form-urlencoded",
                "Authorization": f"Bearer {token}",
            },
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND
        assert response.json() == {"detail": "user with given username does not exist"}


class TestGetUser:
    @pytest.mark.usefixtures("create_user")
    async def test_get_user(self, client_app):
        response = await client_app.get(f"/users/{USER.username}")
        assert response.status_code == status.HTTP_200_OK
        assert response.json()["username"] == USER.username
        assert "salary" not in response.json()
        assert "promotion_date" not in response.json()

    async def test_get_me_user_not_exist(self, client_app):
        response = await client_app.get(
            f"/users/{USER.username}",
            headers={"content-type": "application/x-www-form-urlencoded"},
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND
        assert response.json() == {"detail": "user with given username does not exist"}


class TestUpdateUser:
    UPDATED_DATA = UserUpdate(
        password="12345678",
        salary=5000,
        promotion_date=date.today() + timedelta(1),
    )

    @pytest.mark.usefixtures("create_user")
    async def test_update_user(self, client_app, class_session_factory):
        token = create_access_token(identity=USER.username)
        response = await client_app.patch(
            "/users",
            data=self.UPDATED_DATA.json(),
            headers={
                "Content-Type": "application/json",
                "Authorization": f"Bearer {token}",
            },
        )
        assert response.status_code == status.HTTP_200_OK
        assert response.json()["username"] == USER.username
        assert response.json()["salary"] == self.UPDATED_DATA.salary
        assert response.json()[
            "promotion_date"
        ] == self.UPDATED_DATA.promotion_date.strftime("%Y-%m-%d")
        update_user = await get_user(class_session_factory, USER.username)
        assert update_user.password != USER.password


class TestDeleteUser:
    @pytest.mark.usefixtures("create_user")
    async def test_delete_user(self, client_app, class_session_factory):
        token = create_access_token(identity=USER.username)
        response = await client_app.delete(
            "/users",
            headers={
                "Content-Type": "application/json",
                "Authorization": f"Bearer {token}",
            },
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT
        user = await get_user(class_session_factory, USER.username)
        assert user is None
