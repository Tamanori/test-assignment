import asyncio
import datetime
import pytest
import pytest_asyncio
from sqlalchemy import text
import sqlalchemy
from sqlalchemy.ext.asyncio import async_sessionmaker, create_async_engine
from src.core.config import get_settings
from src.db.base import Base
from src.schemas.user import UserIn


USER = UserIn(
    username="test",
    password="password",
    salary=1000,
    promotion_date=datetime.date.today(),
)
TEST_DATABASE_NAME = "test_database"


@pytest.fixture(scope="session")
def settings():
    return get_settings()


@pytest.fixture(scope="module")
def event_loop():
    loop = asyncio.new_event_loop()
    yield loop
    loop.close()


@pytest_asyncio.fixture(scope="module")
async def test_engine(settings):
    engine_aux = create_async_engine(
        settings.postgres_url(),
        future=True,
    )
    await create_db(engine_aux)
    engine = create_async_engine(
        settings.postgres_url(TEST_DATABASE_NAME),
        future=True,
        echo=False,
    )
    try:
        yield engine
    finally:
        await drop_db(engine_aux)


@pytest_asyncio.fixture
async def class_session_factory(test_engine):
    await create_tables(test_engine)
    try:
        yield async_sessionmaker(test_engine, expire_on_commit=False)
    finally:
        await drop_tables(test_engine)


async def create_db(engine) -> None:
    async with engine.connect() as conn:
        await conn.execution_options(isolation_level="AUTOCOMMIT")
        try:
            await conn.execute(text(f"create database {TEST_DATABASE_NAME}"))
        except sqlalchemy.exc.ProgrammingError:
            await conn.execute(
                text(f"drop database if exists {TEST_DATABASE_NAME} WITH (FORCE)")
            )
            await conn.execute(text(f"create database {TEST_DATABASE_NAME}"))


async def create_tables(engine) -> None:
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)


async def drop_tables(engine) -> None:
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)


async def drop_db(engine) -> None:
    async with engine.connect() as conn:
        await conn.execution_options(isolation_level="AUTOCOMMIT")
        await conn.execute(
            text(f"drop database if exists {TEST_DATABASE_NAME} WITH (FORCE)")
        )
        await engine.dispose()
