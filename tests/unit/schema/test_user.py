from src.schemas.user import UserCreate, UserUpdate
from src.utils.auth import verify_password


def test_user_create():
    user_update = UserCreate(password="test", username="test")
    assert verify_password("test", user_update.password)


def test_user_update():
    user_update = UserUpdate(password="test")
    assert verify_password("test", user_update.password)
    assert hasattr(user_update, "updated_at")


def test_user_update_empty():
    user_update = UserUpdate()
    assert user_update.password == None
    assert hasattr(user_update, "updated_at")
