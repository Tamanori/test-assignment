from datetime import datetime
import pytest
from typing import Any
from src.db.models.user import UserTable
from src.db.repository.abc import T, AbstractRepository
from src.db.unit_of_work import AbstractUnitOfWork, UowT
from src.schemas.user import UserIn
from src.utils.auth import get_password_hash


class FakeRepository(AbstractRepository[T]):
    def __init__(self, session: Any) -> None:
        self.session = []
        self.curr_id = 0

    def add(self, entity: T) -> None:
        self.session.append(entity)

    def get(self, reference):
        try:
            return next(b for b in self.session if b.username == reference)
        except StopIteration:
            return None

    async def get_by_name(self, username):
        if (user := self.get(username)) is not None:
            return user

    async def update_user(self, user_id, updated_data):
        user = await self.get_by_id(user_id)
        if updated_data.password:
            user.password = updated_data.password
        if updated_data.salary:
            user.salary = updated_data.salary
        if updated_data.promotion_date:
            user.promotion_date = updated_data.promotion_date
        user.updated_at = updated_data.updated_at
        return user

    async def get_by_id(self, id):
        try:
            return next(b for b in self.session if b.id == id)
        except StopIteration:
            return None

    async def delete_user(self, user_id):
        user = await self.get_by_id(user_id)
        self.session.remove(user)

    def count(self):
        return len(self.session)

    def create(self, entity):
        self.curr_id += 1
        return UserTable(
            username=entity.username,
            password=get_password_hash(entity.password),
            id=self.curr_id,
            updated_at=datetime.utcnow(),
            created_at=datetime.utcnow(),
        )


class FakeUnitOfWork(AbstractUnitOfWork[UowT]):
    def __init__(self):
        self.repo = FakeRepository([])
        self.committed = False

    async def commit(self):
        self.committed = True

    async def rollback(self):
        pass

    async def refresh(self):
        pass


@pytest.fixture
def uow():
    return FakeUnitOfWork()
