from datetime import timedelta
import time
import pytest
from src.core.exceptions import TokenExpired, ValidateCredentials
from src.utils.auth import (
    create_access_token,
    create_refresh_token,
    decode_access_token,
    decode_refresh_token,
    get_password_hash,
    verify_password,
)
from tests.conftest import USER

TEST_PASS = USER.password


def test_get_password_hash():
    hash_pass = get_password_hash(TEST_PASS)
    assert isinstance(hash_pass, str)


class TestVerifyPassword:
    def test_verify_password(self):
        hash_pass = get_password_hash(TEST_PASS)
        result = verify_password(TEST_PASS, hash_pass)
        assert result is True

    def test_verify_password_fail(self):
        hash_pass = get_password_hash(TEST_PASS)
        result = verify_password("wrong_password", hash_pass)
        assert result is False

    def test_verify_password_wrong_type(self):
        hash_pass = get_password_hash(TEST_PASS)
        with pytest.raises(TypeError):
            verify_password(1, hash_pass)


def test_create_access_token():
    access_token = create_access_token(identity=USER.username)
    assert isinstance(access_token, str)


def test_create_refresh_token():
    refresh_token = create_refresh_token(identity=USER.username)
    assert isinstance(refresh_token, str)


class TestDecodeAccessToken:
    def test_decode_access_token(self):
        token = create_access_token(identity=USER.username)
        result = decode_access_token(token)
        assert result == USER.username

    def test_decode_access_token_wrong_type(self):
        with pytest.raises(AttributeError):
            decode_access_token(123)

    def test_decode_access_token_wrong_value(self):
        token = create_access_token(identity=USER.username)
        with pytest.raises(ValidateCredentials):
            decode_access_token(token + "123")

    def test_decode_access_token_expired(self):
        token = create_access_token(
            identity=USER.username, expires_delta=timedelta(seconds=1)
        )
        time.sleep(1)
        with pytest.raises(TokenExpired):
            decode_access_token(token)


class TestDecodeRefreshToken:
    def test_decode_refresh_token(self):
        token = create_refresh_token(identity=USER.username)
        result = decode_refresh_token(token)
        assert result == USER.username

    def test_decode_refresh_token_wrong_type(self):
        with pytest.raises(AttributeError):
            decode_refresh_token(123)

    def test_decode_refresh_token_wrong_value(self):
        token = create_refresh_token(identity=USER.username)
        with pytest.raises(ValidateCredentials):
            decode_refresh_token(token + "123")

    def test_decode_refresh_token_expired(self):
        token = create_refresh_token(
            identity=USER.username, expires_delta=timedelta(seconds=1)
        )
        time.sleep(1)
        with pytest.raises(TokenExpired):
            decode_refresh_token(token)
