import pytest
from src.core.exceptions import UserNotExist, ValidateCredentials
from src.schemas.auth import Token
from src.service.auth import authenticate_user, refresh_token_check
from src.utils.auth import create_refresh_token, get_password_hash
from tests.conftest import USER


class TestAuthentificateUser:
    async def test_auth_user_not_exist_user(self, uow):
        with pytest.raises(UserNotExist):
            await authenticate_user(USER, uow)

    async def test_auth_user_wrong_pass(self, uow):
        created_user = uow.repo.create(USER)
        uow.repo.add(created_user)
        USER.password = get_password_hash("wrong_pass")
        with pytest.raises(ValidateCredentials):
            await authenticate_user(USER, uow)

    async def test_auth_user(self, uow):
        created_user = uow.repo.create(USER)
        uow.repo.add(created_user)
        auth = await authenticate_user(USER, uow)
        assert auth == USER.username


class TestRefreshTokenCheck:
    async def test_refresh_token_user_not_exist(self, uow):
        token = Token(
            access_token="ss",
            refresh_token=create_refresh_token(identity=USER.username),
        )
        with pytest.raises(UserNotExist):
            await refresh_token_check(token, uow)

    async def test_refresh_token(self, uow):
        created_user = uow.repo.create(USER)
        uow.repo.add(created_user)
        token = Token(
            access_token="ss",
            refresh_token=create_refresh_token(identity=USER.username),
        )
        result = await refresh_token_check(token, uow)
        assert result.username == USER.username
