from datetime import datetime, timedelta
from jose import jwt
from fastapi import HTTPException
import pytest
from src.schemas.user import UserUpdate
from src.service.user import (
    create_new_user,
    delete_user_service,
    get_current_user,
    update_user_service,
)
from src.utils.auth import create_access_token
from tests.conftest import USER


class TestCreateUser:
    async def test_create_user(self, uow):
        await create_new_user(USER, uow)
        assert uow.repo.get(USER.username) is not None
        assert uow.repo.get(USER.username).username == USER.username
        assert uow.committed == True

    async def test_create_user_already_exist(self, uow):
        created_user = uow.repo.create(USER)
        uow.repo.add(created_user)
        with pytest.raises(HTTPException) as error:
            await create_new_user(USER, uow)
        assert error.value.detail == "user or email already exist"


class TestGetCurrentUser:
    async def test_get_current_user(self, uow):
        created_user = uow.repo.create(USER)
        uow.repo.add(created_user)
        token = create_access_token(identity=USER.username)
        cur_user = await get_current_user(token, uow)
        assert cur_user.username == USER.username

    async def test_get_current_user_wrong_token_scheme(self, uow, settings):
        created_user = uow.repo.create(USER)
        uow.repo.add(created_user)
        token = jwt.encode(
            {"exp": datetime.now() + timedelta(minutes=30)},
            settings.SECRET_KEY,
            algorithm=settings.ALGORITHM,
        )
        with pytest.raises(HTTPException):
            await get_current_user(token, uow)

    async def test_get_current_user_user_no_exist(self, uow, settings):
        token = jwt.encode(
            {"exp": datetime.now() + timedelta(minutes=30)},
            settings.SECRET_KEY,
            algorithm=settings.ALGORITHM,
        )
        with pytest.raises(HTTPException):
            await get_current_user(token, uow)


class TestUpdateCurrentUser:
    async def test_update_current_user(self, uow):
        created_user = uow.repo.create(USER)
        uow.repo.add(created_user)
        old_upd_time = created_user.updated_at
        updated_data = UserUpdate(password="12345678")
        await update_user_service(updated_data, created_user, uow)
        user = uow.repo.get(created_user.username)
        assert user.password == updated_data.password
        assert user.updated_at > old_upd_time
        assert uow.committed == True

    async def test_update_current_user_empty_body(self, uow):
        created_user = uow.repo.create(USER)
        uow.repo.add(created_user)
        old_upd_time = created_user.updated_at
        await update_user_service(UserUpdate(), created_user, uow)
        user = uow.repo.get(created_user.username)
        assert user.password == created_user.password
        assert user.updated_at > old_upd_time
        assert uow.committed == True


class TestDeleteUser:
    async def test_delete_user(self, uow):
        created_user = uow.repo.create(USER)
        uow.repo.add(created_user)
        assert uow.repo.get(created_user.username) is not None
        await delete_user_service(created_user, uow)
        assert uow.repo.get(created_user.username) is None
        assert uow.committed == True
