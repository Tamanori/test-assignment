# pylint: disable=missing-class-docstring
from functools import lru_cache
from pydantic import BaseSettings


class Settings(BaseSettings):
    """Base settings for database"""

    # DATABASE SETTINGS
    POSTGRES_USER: str
    POSTGRES_PASSWORD: str
    POSTGRES_NAME: str
    POSTGRES_HOST: str
    POSTGRES_PORT: int
    # JWT SETTINGS
    SECRET_KEY: str
    REFRESH_SECRET_KEY: str
    ALGORITHM: str
    ACCESS_TOKEN_EXPIRE_MINUTES: int
    REFRESH_TOKEN_EXPIRE_MINUTES: int

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"
        case_sensitive = False

    def postgres_url(self, database_name: str | None = None) -> str:
        return (
            f"postgresql+asyncpg://"
            f"{self.POSTGRES_USER}:{self.POSTGRES_PASSWORD}"
            f"@{self.POSTGRES_HOST}:{self.POSTGRES_PORT}/"
            f"{self.POSTGRES_NAME if not database_name else database_name}"
        )


@lru_cache()
def get_settings() -> Settings:
    return Settings()  # type:ignore
