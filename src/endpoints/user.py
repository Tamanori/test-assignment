from fastapi import APIRouter, Depends, status
from src.schemas.auth import Token
from src.utils.auth import create_access_token, create_refresh_token
from src.service.user import (
    create_new_user,
    delete_user_service,
    get_current_user,
    get_user_service,
    update_user_service,
)
from src.schemas.user import UserOut, UserOutAnon

router = APIRouter(
    prefix="/users",
    tags=["Users"],
)


@router.post(
    "",
    status_code=status.HTTP_201_CREATED,
    response_model=Token,
    summary="Create new user",
    response_description="User data that has beed created",
)
async def create_user(user: UserOut = Depends(create_new_user)) -> Token:
    """
    Create new user. You need to provide unique username and password, other data is optional.

    - **username**: Put unique username
    - **password**: Each user must have password
    - **salary**: Salary by default is 0, but you can make it whatever you want or just erase,
      then it will be None value.
    - **promotion_date**:  By default is today, but you can make it whenever you want or just erase,
      then it will be None value.
    """
    access_token = create_access_token(identity=user.username)
    refresh_token = create_refresh_token(identity=user.username)
    return Token(access_token=access_token, refresh_token=refresh_token)


@router.get(
    "/me",
    summary="Get details of currently authorized user",
    response_model=UserOut,
    response_description="User data",
)
async def read_users_me(current_user: UserOut = Depends(get_current_user)) -> UserOut:
    """
    Get the details of the current authorized user. The difference between retrieving data
    using this method and retrieving data by username is that this method includes personal
    data such as salary and date of promotion, while these data are not included in the
    method when you retrieve data by username for privacy reasons.

    """
    return current_user


@router.get(
    "/{username}",
    response_model=UserOutAnon,
    summary="Get details of a user by username",
    response_description="User data",
)
async def get_user(user: UserOutAnon = Depends(get_user_service)) -> UserOutAnon:
    """
    General use method for getting details of a user by username.
    It proived public data given user, but hides personal data like salary or promotion date.
    """
    return user


@router.patch(
    "",
    response_model=UserOut,
    status_code=status.HTTP_200_OK,
    summary="Update details of currently authorized user",
    response_description="User data",
)
async def update_user(
    updated_data: UserOut = Depends(update_user_service),
) -> UserOut:
    """
    Update details of currently authorized user.
        All fields are optional, you may enter no data, in thit case only 'updated_at' field will be updated.
    - **password**: Put new password
    - **salary**: Salary by default is 0, but you can make it whatever you want.
    - **promotion_date**:  By default is today, but you can make it whenever you want.

    """
    return updated_data


@router.delete("", status_code=status.HTTP_204_NO_CONTENT, summary="Delete user")
async def delete_user(
    _: None = Depends(delete_user_service),
) -> None:
    """
    Delete currently authorized user.
    """
    pass
