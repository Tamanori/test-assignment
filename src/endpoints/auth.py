from fastapi import APIRouter, Depends
from src.schemas.auth import Token
from src.utils.auth import (
    create_access_token,
    create_refresh_token,
)
from src.service.auth import authenticate_user, refresh_token_check
from src.schemas.user import UserOut

router = APIRouter(
    prefix="/auth",
    tags=["Auth"],
)


@router.post(
    "/token",
    response_model=Token,
    summary="Generate access and refresh token",
    response_description="The created tokens",
)
async def get_user_token(username: str = Depends(authenticate_user)) -> Token:
    """
    Create jwt token for the user:

    - **username**: Each user have unique username
    - **password**: Each user have password
    """
    access_token = create_access_token(identity=username)
    refresh_token = create_refresh_token(identity=username)
    return Token(access_token=access_token, refresh_token=refresh_token)


@router.post(
    "/refresh",
    response_model=Token,
    summary="Refresh access and refresh token",
    response_description="The created tokens",
)
async def refresh_token(user: UserOut = Depends(refresh_token_check)) -> Token:
    """
    Refresh jwt token for the user using the refresh token. It also refresh your refresh token.

    - **grant_type**: You need to provide the grant type. By default it's 'refresh token' and you need to let it as it is.
    - **refresh_token**: Put your refrresh token here.
    """
    access_token = create_access_token(identity=user.username)
    refresh_token = create_refresh_token(identity=user.username)
    return Token(access_token=access_token, refresh_token=refresh_token)
