from jose import jwt, JWTError
from typing import TypedDict
from datetime import datetime, timedelta
from fastapi import Form
from fastapi.security import OAuth2PasswordBearer
from passlib.context import CryptContext
from src.core.config import get_settings
import src.core.exceptions as exc

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="auth/token", scheme_name="JWT")

settings = get_settings()


class PayloadType(TypedDict):
    sub: str
    exp: float


class OAuth2RefreshRequestForm:
    """Modified from fastapi.security.OAuth2PasswordRequestForm"""

    def __init__(
        self,
        grant_type: str = Form(default="refresh_token", regex="refresh_token"),
        refresh_token: str = Form(),
    ):
        self.grant_type = grant_type
        self.refresh_token = refresh_token


def get_password_hash(password: str) -> str:
    hash_pass: str = pwd_context.hash(password)
    return hash_pass


def verify_password(plain_password: str, hashed_password: str) -> bool:
    verify: bool = pwd_context.verify(plain_password, hashed_password)
    return verify


def create_access_token(
    identity: str,
    expires_delta: timedelta | None = None,
) -> str:
    expire = datetime.utcnow() + (
        expires_delta
        if expires_delta
        else timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    )
    claims = {"sub": identity, "exp": expire}
    encoded_jwt: str = jwt.encode(
        claims, settings.SECRET_KEY, algorithm=settings.ALGORITHM
    )
    return encoded_jwt


def create_refresh_token(
    identity: str,
    expires_delta: timedelta | None = None,
) -> str:
    expire = datetime.utcnow() + (
        expires_delta
        if expires_delta
        else timedelta(minutes=settings.REFRESH_TOKEN_EXPIRE_MINUTES)
    )
    claims = {"sub": identity, "exp": expire}
    encoded_jwt: str = jwt.encode(
        claims, settings.REFRESH_SECRET_KEY, settings.ALGORITHM
    )
    return encoded_jwt


def decode_access_token(token: str) -> str:
    try:
        payload: PayloadType = jwt.decode(
            token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM]
        )
        if (username := payload.get("sub")) is None:
            raise exc.ValidateCredentials()
    except JWTError:
        raise exc.ValidateCredentials()
    assert (exp := payload.get("exp"))
    if datetime.fromtimestamp(exp) < datetime.now():
        raise exc.TokenExpired()
    return username


def decode_refresh_token(
    token: str,
) -> str:
    try:
        payload: PayloadType = jwt.decode(
            token, settings.REFRESH_SECRET_KEY, algorithms=[settings.ALGORITHM]
        )
        if (username := payload.get("sub")) is None:
            raise exc.ValidateCredentials()
    except JWTError:
        raise exc.ValidateCredentials()
    assert (exp := payload.get("exp"))
    if datetime.fromtimestamp(exp) < datetime.now():
        raise exc.TokenExpired(detail="Refresh token expired")
    return username
