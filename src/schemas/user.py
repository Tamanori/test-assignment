from datetime import datetime, date
from pydantic import BaseModel, Field, root_validator, validator

from src.utils.auth import get_password_hash


class UserBase(BaseModel):
    username: str
    salary: float | None
    promotion_date: date | None


class UserIn(UserBase):
    password: str

    class Config:
        orm_mode = True


class UserCreate(UserBase):
    password: str
    created_at: datetime = Field(default_factory=datetime.utcnow)
    updated_at: datetime = Field(default_factory=datetime.utcnow)

    # validators
    _convert_pass_to_hash = validator("password", allow_reuse=True)(get_password_hash)


class UserOut(UserBase):
    id: int
    created_at: datetime
    updated_at: datetime

    class Config:
        orm_mode = True


class UserOutAnon(BaseModel):
    id: int
    username: str
    created_at: datetime
    updated_at: datetime

    class Config:
        orm_mode = True


class UserUpdate(BaseModel):
    password: str | None
    salary: float | None
    promotion_date: date | None

    # validators
    _convert_pass_to_hash = validator("password", allow_reuse=True)(get_password_hash)

    @root_validator
    def add_updated_field(
        cls, values: dict[str, str | float | date | datetime]
    ) -> dict[str, str | float | date | datetime]:
        values["updated_at"] = datetime.utcnow()
        return values
