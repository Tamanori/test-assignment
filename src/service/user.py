from fastapi import Depends
from src.utils.auth import (
    oauth2_scheme,
    decode_access_token,
)
import src.core.exceptions as exc
from src.schemas.user import (
    UserOut,
    UserIn,
    UserCreate,
    UserOutAnon,
    UserUpdate,
)
from src.db.unit_of_work import uow_context_manager, SqlAlchemyUnitOfWork
from src.db.repository.user import UserRepository


async def create_new_user(
    user_data: UserIn,
    uow: SqlAlchemyUnitOfWork[UserRepository] = Depends(
        uow_context_manager(UserRepository)
    ),
) -> UserOut:
    """Service to create a new user

    Args:
        user_data (UserIn): Must include username and password, other are optional.

    Raises:
        exc.UserExist: Error in case if user with given username already exists.

    """
    if await uow.repo.get_by_name(username=user_data.username):
        raise exc.UserExist()
    new_user = uow.repo.create(UserCreate(**user_data.dict()))
    uow.repo.add(new_user)
    await uow.commit()
    return UserOut.from_orm(new_user)


async def get_current_user(
    token: str = Depends(oauth2_scheme),
    uow: SqlAlchemyUnitOfWork[UserRepository] = Depends(
        uow_context_manager(UserRepository)
    ),
) -> UserOut:
    """Service to get the current user

    Args:
        token (str, optional): User access token

    Raises:
        exc.UserNotExist: if username doesnt exist in database.

    Returns:
        UserOut: Returns both public and personal data.
    """
    username = decode_access_token(token)
    if (user := await uow.repo.get_by_name(username)) is None:
        raise exc.UserNotExist()
    return UserOut.from_orm(user)


async def get_user_service(
    username: str,
    uow: SqlAlchemyUnitOfWork[UserRepository] = Depends(
        uow_context_manager(UserRepository)
    ),
) -> UserOutAnon:
    """Service to get user by username

    Args:
        username (str)

    Raises:
        exc.UserNotExist: if username doesnt exist in database.

    Returns:
        UserOutAnon: Returns only public avaialble user data
    """
    if (user := await uow.repo.get_by_name(username)) is None:
        raise exc.UserNotExist()
    return UserOutAnon.from_orm(user)


async def update_user_service(
    user_data: UserUpdate,
    user: UserOut = Depends(get_current_user),
    uow: SqlAlchemyUnitOfWork[UserRepository] = Depends(
        uow_context_manager(UserRepository)
    ),
) -> UserOut:
    """_summary_

    Args:
        user_data (UserUpdate): Data to update.
        user (UserOut, optional): Currently login user.

    Returns:
        UserOut: _description_
    """
    updated_data = await uow.repo.update_user(user.id, user_data)
    await uow.commit()
    return UserOut.from_orm(updated_data)


async def delete_user_service(
    user: UserOut = Depends(get_current_user),
    uow: SqlAlchemyUnitOfWork[UserRepository] = Depends(
        uow_context_manager(UserRepository)
    ),
) -> None:
    """Service to delete a user

    Args:
        user (UserOut, optional): Currently login user.
    """
    await uow.repo.delete_user(user.id)
    await uow.commit()
