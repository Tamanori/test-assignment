from fastapi import Depends
from fastapi.security import OAuth2PasswordRequestForm
from src.utils.auth import (
    OAuth2RefreshRequestForm,
    verify_password,
    decode_refresh_token,
)
import src.core.exceptions as exc
from src.schemas.user import UserOut
from src.db.unit_of_work import uow_context_manager, SqlAlchemyUnitOfWork
from src.db.repository.user import UserRepository


async def authenticate_user(
    form_data: OAuth2PasswordRequestForm = Depends(),
    uow: SqlAlchemyUnitOfWork[UserRepository] = Depends(
        uow_context_manager(UserRepository)
    ),
) -> str:
    """Service function to authenticate user

    Args:
        form_data (OAuth2PasswordRequestForm, optional): Form object that must include user and password.

    Raises:
        exc.UserNotExist: raises when user does not exist.
        exc.ValidateCredentials: raises when password does not match or invalid.

    Returns:
        str: Username of authenticated user.
    """
    if (user := await uow.repo.get_by_name(form_data.username)) is None:
        raise exc.UserNotExist()
    if not verify_password(form_data.password, user.password):
        raise exc.ValidateCredentials()
    return user.username


async def refresh_token_check(
    token: OAuth2RefreshRequestForm = Depends(),
    uow: SqlAlchemyUnitOfWork[UserRepository] = Depends(
        uow_context_manager(UserRepository)
    ),
) -> UserOut:
    """Service function to check refresh token validity

    Args:
        token (OAuth2RefreshRequestForm, optional): Refresh token form data.
        It includes grant type(by default 'refresh_token') and refresh token. itself.

    Raises:
        exc.UserNotExist: User not exist_

    Returns:
        UserOut
    """
    username = decode_refresh_token(token.refresh_token)
    if (user := await uow.repo.get_by_name(username)) is None:
        raise exc.UserNotExist()
    return UserOut.from_orm(user)
