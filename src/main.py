from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from src.endpoints import user, auth

DESCRIPTION = """
Shift api for getting user data with jwt tokens implementation. 🚀

First step is to create a user account, after that you'll be able to authorize your account and make actions.

Since the task didn't required how employee data should be stored in database,
a chose the most basic and simple choise - store that data in the main user table,
although i think most preferabble way would be separate these types of data.

Also, the task doesn't say a word about how we need to assign a salary and a promotion date to an employee,
so for simplicity, I simplified the task - the user himself can set a salary and a raise date.
But of cource, in real time scenario, it would be better to create users with different roles
where seniors can set these types of data, not ordinary workers.
"""
TAGS_METADATA = [
    {
        "name": "Users",
        "description": "Operations with users.",
    },
    {
        "name": "Auth",
        "description": "Route for operating with jwt tokens",
    },
]


def create_app() -> FastAPI:
    app = FastAPI(
        title="SHIFT API test assignment",
        description=DESCRIPTION,
        version="1.0.0",
        openapi_tags=TAGS_METADATA,
    )
    app.include_router(user.router)
    app.include_router(auth.router)
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    return app
