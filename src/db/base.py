from typing import AsyncGenerator
from fastapi import Depends
from sqlalchemy.ext.asyncio import (
    AsyncEngine,
    AsyncSession,
    create_async_engine,
    async_sessionmaker,
    AsyncAttrs,
)
from sqlalchemy.orm import DeclarativeBase
from src.core.config import get_settings, Settings


class Base(AsyncAttrs, DeclarativeBase):  # pylint:disable=missing-class-docstring
    pass


async def create_db_engine(
    settings: Settings = Depends(get_settings),
) -> AsyncGenerator[AsyncEngine, None]:
    engine = create_async_engine(
        settings.postgres_url(),
        future=True,
        echo=True,
    )
    yield engine
    await engine.dispose()


def create_db_session(
    engine: AsyncEngine = Depends(create_db_engine),
) -> async_sessionmaker[AsyncSession]:
    return async_sessionmaker(engine, expire_on_commit=False)


async def async_db_session(
    session: async_sessionmaker[AsyncSession] = Depends(create_db_session),
) -> AsyncGenerator[AsyncSession, None]:
    async with session() as session_obj:
        yield session_obj
