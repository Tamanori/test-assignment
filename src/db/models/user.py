from datetime import datetime
from sqlalchemy.orm import Mapped, mapped_column
from sqlalchemy import String, func
from src.db.base import Base


class UserTable(Base):
    __tablename__ = "user_table"
    id: Mapped[int] = mapped_column(primary_key=True)
    username: Mapped[str] = mapped_column(String(32), nullable=False, unique=True)
    password: Mapped[str] = mapped_column(nullable=False)
    created_at: Mapped[datetime] = mapped_column(insert_default=func.now())
    updated_at: Mapped[datetime] = mapped_column(insert_default=func.now())
    salary: Mapped[float] = mapped_column(nullable=True)
    promotion_date: Mapped[datetime] = mapped_column(nullable=True)
