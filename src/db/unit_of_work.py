import abc
from typing import Any, AsyncGenerator, Callable, Generic, TypeVar
from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession
from src.db.base import async_db_session


UowT = TypeVar("UowT")


class AbstractUnitOfWork(abc.ABC, Generic[UowT]):
    """Abstract class for Unit of Work"""

    def __init__(self, session: Any) -> None:
        self.session = session

    def __call__(self, repo: Callable[..., UowT]) -> "AbstractUnitOfWork[UowT]":
        self.repo = repo(self.session)
        return self

    async def __aenter__(self) -> "AbstractUnitOfWork[UowT]":
        return self

    async def __aexit__(self, *args: Any) -> None:
        pass

    @abc.abstractmethod
    async def commit(self) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    async def rollback(self) -> None:
        raise NotImplementedError


class SqlAlchemyUnitOfWork(AbstractUnitOfWork[UowT]):
    """SqlAlchemy instance unit of work"""

    def __init__(
        self,
        session: AsyncSession = Depends(async_db_session),
    ) -> None:
        super().__init__(session)

    def __call__(self, repo: Callable[..., UowT]) -> AbstractUnitOfWork[UowT]:
        self.repo = repo(self.session)
        return self

    async def commit(self) -> None:
        await self.session.commit()

    async def rollback(self) -> None:
        self.session.rollback()


def uow_context_manager(
    repo: Callable[..., UowT],
) -> Callable[
    [AbstractUnitOfWork[UowT]], AsyncGenerator[AbstractUnitOfWork[UowT], None]
]:
    """
    Context  manager for calling UnitOfWork instance

    Args:
        repo (Callable[..., UowT]): repository instance
    """

    async def wrapper(
        uow: AbstractUnitOfWork[UowT] = Depends(SqlAlchemyUnitOfWork),
    ) -> AsyncGenerator[AbstractUnitOfWork[UowT], None]:
        async with uow(repo) as session:
            yield session

    return wrapper
