from sqlalchemy import delete, select, update
from src.db.repository.abc import SqlAlchemyRepository
from src.schemas.user import UserCreate, UserUpdate
from src.db.models.user import UserTable


class UserRepository(SqlAlchemyRepository[UserTable]):
    """
    User repository with operations on user table.

    """

    async def get_by_name(self, username: str) -> UserTable | None:
        query = select(UserTable).where(UserTable.username == username)
        results = await self.session.execute(query)
        user: UserTable | None = results.scalar()
        return user

    async def update_user(self, user_id: int, updated_data: UserUpdate) -> UserTable:
        """
        Update user by id.

        Args:
            user_id (int)
            updated_data (UserUpdate)

        Returns:
            UserTable: instance with updated data.
        """
        returned_data = [
            getattr(UserTable, field)
            for field in (
                column
                for column in UserTable.__table__.columns.keys()
                if column != "password"
            )
        ]
        query = (
            update(UserTable)
            .where(UserTable.id == user_id)
            .values(**updated_data.dict(exclude_none=True))
            .returning(*returned_data)
        )
        results = await self.session.execute(query)
        user: UserTable = results.first()
        return user

    async def delete_user(self, user_id: int) -> None:
        """Delete user by id.

        Args:
            user_id (int)
        """
        query = delete(UserTable).where(UserTable.id == user_id)
        await self.session.execute(query)

    def create(self, user: UserCreate) -> UserTable:
        """Create user.

        Args:
            user (UserCreate)

        Returns:
            UserTable:
        """
        return UserTable(**user.dict())
