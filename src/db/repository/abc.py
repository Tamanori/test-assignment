from abc import ABC, abstractmethod
from typing import Any, Generic, TypeVar


T = TypeVar("T")


class AbstractRepository(ABC, Generic[T]):
    """
    Abstract Repository wiith base for concrete implementation.

    Args:
        Generic (_type_): Type of table that you'll be using.
    """

    def __init__(self, session: Any) -> None:
        self.session = session

    @abstractmethod
    def add(self, entity: Any) -> None:
        ...


class SqlAlchemyRepository(AbstractRepository[T]):
    """
    SqlAlchemy Repository.

    Args:
        Generic (_type_): Type of table that you'll be using.
    """

    def add(self, entity: Any) -> None:
        self.session.add(entity)
